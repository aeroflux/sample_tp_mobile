package ekspedia.id.yudanto_1202150097_modul2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

public class DineActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    EditText inputNama;
    Spinner spinner;

    String pilihan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dine);
        inputNama = findViewById(R.id.inputNameDine);
        spinner = findViewById(R.id.spinMeja);

        // Create ArrayAdapter using the string array and default spinner layout.
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.arr_no_meja, android.R.layout.simple_spinner_item);

        // Specify the layout to use when the list of choices appears.
        adapter.setDropDownViewResource
                (android.R.layout.simple_spinner_dropdown_item);

        if (spinner != null) {
            spinner.setOnItemSelectedListener(this);
            spinner.setAdapter(adapter);
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        pilihan = adapterView.getItemAtPosition(i).toString();
        Log.d("On Item Selected", "You select " + pilihan + "!");
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {
        Log.d("On Nothing Selected", "You select nothing!");
    }

    public void takeDine(View view) {
        Intent intent = new Intent(this, MenuActivity.class);
        startActivity(intent);
    }
}
