package ekspedia.id.yudanto_1202150097_modul2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class DetailActivity extends AppCompatActivity {

    TextView textTitle, textSub;
    ImageView imgFood;

    String title, sub;
    int image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        Intent intent = getIntent();
        title = intent.getStringExtra("title");
        sub = intent.getStringExtra("subtitle");
        image = intent.getIntExtra("image", 0);

        textTitle = findViewById(R.id.textTitle);
        textSub = findViewById(R.id.textSub);
        imgFood = findViewById(R.id.imgFoo);

        textTitle.setText(title);
        textSub.setText(sub);
        imgFood.setImageResource(image);
    }
}
