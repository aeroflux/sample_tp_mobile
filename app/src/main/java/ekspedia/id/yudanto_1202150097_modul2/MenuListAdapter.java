package ekspedia.id.yudanto_1202150097_modul2;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Yuda on 2/17/2018.
 */

public class MenuListAdapter extends RecyclerView.Adapter<MenuListAdapter.MenuViewHolder> {
    private final ArrayList<String> mTitleList;
    private final ArrayList<String> mSubList;
    private final ArrayList<Integer> mImgList;
    private LayoutInflater mInflater;
    private Context context;

    public MenuListAdapter(Context context, ArrayList<String> wordList,
                           ArrayList<String> subList,
                           ArrayList<Integer> imgList) {
        mInflater = LayoutInflater.from(context);
        this.mTitleList = wordList;
        this.mSubList = subList;
        this.mImgList = imgList;
    }

    @Override
    public MenuViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View mItemView = mInflater.inflate(R.layout.item_menu_list, parent, false);
        return new MenuViewHolder(mItemView, this);
    }

    @Override
    public void onBindViewHolder(MenuViewHolder holder, int position) {
        String title = mTitleList.get(position);
        String subtitle = mSubList.get(position);
        int image = mImgList.get(position);
        holder.itemTitle.setText(title);
        holder.itemSub.setText(subtitle);
        holder.itemImage.setImageResource(image);
    }

    @Override
    public int getItemCount() {
        return mTitleList.size();
    }

    class MenuViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public final TextView itemTitle;
        public final TextView itemSub;
        public final ImageView itemImage;
        final MenuListAdapter mAdapter;

        public MenuViewHolder(View itemView, MenuListAdapter adapter) {
            super(itemView);
            itemTitle = itemView.findViewById(R.id.itemTitle);
            itemSub = itemView.findViewById(R.id.itemSub);
            itemImage = itemView.findViewById(R.id.itemPic);
            this.mAdapter = adapter;
            context = itemView.getContext();
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            // Get the position of the item that was clicked.
            int mPosition = getLayoutPosition();
            // Use that to access the affected item in mWordList.
            String getTitle = mTitleList.get(mPosition);
            String getSub = mSubList.get(mPosition);
            int getImage = mImgList.get(mPosition);

            Intent intent = new Intent(context, DetailActivity.class);
            intent.putExtra("title", getTitle);
            intent.putExtra("subtitle", getSub);
            intent.putExtra("image", getImage);
            context.startActivity(intent);

            // Notify the adapter, that the data has changed so it can
            // update the RecyclerView to display the data.
            mAdapter.notifyDataSetChanged();
        }
    }
}
