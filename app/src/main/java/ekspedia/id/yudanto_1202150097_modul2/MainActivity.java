package ekspedia.id.yudanto_1202150097_modul2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    RadioButton optDineIn, optTakeAway;
    Button btnPesan;

    // mengetahui pilihan
    String pilihan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        optDineIn = findViewById(R.id.optDineIn);
        optTakeAway = findViewById(R.id.optTakeAway);
        btnPesan = findViewById(R.id.btnPesan);

        optDineIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pilihan = "Dine In";
            }
        });

        optTakeAway.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pilihan = "Take Away";
            }
        });
    }

    public void takeMe(View view) {
        switch (view.getId()) {
            case R.id.btnPesan:
                Toast.makeText(this, "Pilihan : " + pilihan, Toast.LENGTH_SHORT).show();
                if (pilihan.equals("Dine In")) {
                    Intent intent = new Intent(this, DineActivity.class);
                    startActivity(intent);
                } else if (pilihan.equals("Take Away")){
                    Intent intent = new Intent(this, TakeActivity.class);
                    startActivity(intent);
                }
                break;
            default:
                break;
        }
    }
}
