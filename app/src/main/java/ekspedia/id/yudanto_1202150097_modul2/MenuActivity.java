package ekspedia.id.yudanto_1202150097_modul2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;

public class MenuActivity extends AppCompatActivity {
    ArrayList<String> title = new ArrayList<>();
    ArrayList<String> sub = new ArrayList<>();
    ArrayList<Integer> image = new ArrayList<>();

    RecyclerView recyclerView;
    MenuListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        // title
        title.add("Ayam Bakar");
        title.add("Cream Soup");
        title.add("Frappucino");
        title.add("Gulai Cumi");
        title.add("Jus Jambu");
        title.add("Sate");
        title.add("Soto Kudus");
        title.add("Spaghetti");
        title.add("Beef Steak");
        title.add("Cah Kangkung");

        // subtitle
        sub.add("Makanan");
        sub.add("Makanan");
        sub.add("Minuman");
        sub.add("Makanan");
        sub.add("Minuman");
        sub.add("Minuman");
        sub.add("Makanan");
        sub.add("Makanan");
        sub.add("Makanan");
        sub.add("Makanan");

        // image
        image.add(R.drawable.menu1);
        image.add(R.drawable.menu2);
        image.add(R.drawable.menu3);
        image.add(R.drawable.menu4);
        image.add(R.drawable.menu5);
        image.add(R.drawable.menu6);
        image.add(R.drawable.menu7);
        image.add(R.drawable.menu8);
        image.add(R.drawable.menu9);
        image.add(R.drawable.menu10);

        // Get a handle to the RecyclerView.
        recyclerView = (RecyclerView) findViewById(R.id.listMenu);
        // Create an adapter and supply the data to be displayed.
        adapter = new MenuListAdapter(this, title, sub, image);
        // Connect the adapter with the RecyclerView.
        recyclerView.setAdapter(adapter);
        // Give the RecyclerView a default layout manager.
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }
}
